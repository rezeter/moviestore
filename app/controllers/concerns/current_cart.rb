module CurrentCart
   extend ActiveSupport::Concern
  
  private
   
   

   def correct_user
      @user = User.find(current_user.id)
      rendirect_to(cart_url) unless current_user?(@user)
   end

   def current_user?(user)
    user == current_user
   end

   def current_user_cart
    "cart#{current_user.id}"
   end

   def cart_movies
	   @cart_ids =  current_user.moviesid 
     @cart_movies = Movie.find(@cart_ids)	
   end

   def order_movies(movieid)
   	 @order_movie = Movie.find(movieid)
   end

   def clearcart(movieid)
   	 $redis.srem current_user_cart, movieid   	
   end

 end