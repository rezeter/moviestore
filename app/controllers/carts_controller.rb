class CartsController < ApplicationController
  before_action :authenticate_user!
  include CurrentCart
  before_action :current_user_cart
  
  respond_to :html, :js

  def show
    if current_user.cart_count<=0 
      redirect_to root_url, notice: "Ваша корзина пустая!"
      return
    end
    cart_movies
  end
 
  def add
    $redis.sadd current_user_cart, params[:movie_id]
    render json: current_user.cart_count, status: 200
  end
 
  def remove
    clearcart(params[:movie_id])
    render json: current_user.cart_count, status: 200
  end
 
end