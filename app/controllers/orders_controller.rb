class OrdersController < ApplicationController
 
 include CurrentCart
 before_action :authenticate_user! 
 before_action :current_user_cart, only: [:show, :new, :create]
 before_action :set_order, only: [:show, :destroy]
 before_action :order_movie, only: [:show]
 before_action :morder, only: [:show]
 before_action :correct_user, only: [:index, :show]
 
  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.where("user_id = ?", current_user.id)
    
  end

  # GET /orders/1
  # GET /orders/1.json

  def show
    @myorder=@order.myorders
  end

  # GET /orders/new
  def new

    if current_user.cart_count<=0 
      redirect_to root_url, notice: "Ваша корзина пустая!"
      return
    end
    
    cart_movies
    @cartsid= current_user_cart
    @order = Order.new
    @count=@order.relationships.count

    
  end


  # GET /orders/1/edit
  def edit
  end

  # POST /orders
  # POST /orders.json
  def create

    cart_movies
    @order = current_user.order.build(order_params)
    
    respond_to do |format|
      if @order.save
        @cart_ids.each do |movie| 
          @order.addrelation(movie)
          clearcart(movie)
        end        
        format.html { redirect_to @order, notice: "Order was successfully created." }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to root_url, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def who_bought

    @orders = Order.all  
    @latest_order = @orders.order(:updated_at).first
    if stale?(@latest_order)
     respond_to do |format|
       format.atom
     end
    end
  end

  private

    def morder
      redirect_to root_url unless @order.user_id==current_user.id
    end

    def order_movie
      @movie = @order.relationships
      redirect_to root_url if @movie.nil? 
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
      rescue
        redirect_to root_url 
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:name, :address, :email, :pay_type)
    end
end
