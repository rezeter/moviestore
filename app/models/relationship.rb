class Relationship < ActiveRecord::Base
  belongs_to :order
  belongs_to :movie
  attr_accessible :order_id, :movie_id
  validates :movie_id, :order_id,  presence: true
end
