class Movie < ActiveRecord::Base
  has_many :relationships, foreign_key: "movie_id"
  has_many :orders, through: :relationships 
  
  def poster
    "http://www.kinopoisk.ru/images/film_big/#{poster_url}"
  end
 
  def imdb
    "http://www.kinopoisk.ru/film/#{imdb_id}/votes/"
  end

  def cart_action(current_user_id)
    if $redis.sismember "cart#{current_user_id}", id
      "Remove from"
    else
      "Add to"
    end
  end

  
end
