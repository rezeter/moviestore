class Order < ActiveRecord::Base
	belongs_to :user
	has_many :relationships, foreign_key: "order_id", dependent: :destroy
	has_many :myorders, through: :relationships, source: :movie 
	validates :name,  presence: true, length:    { maximum: 50 }
	validates :address,:pay_type, :user_id,  presence: true
	default_scope -> { order('created_at DESC') }
	PAYMENT_TYPES = [ "Check", "Credit card", "Purchase order" ]
	validates :pay_type, inclusion: PAYMENT_TYPES
	attr_accessible :name, :address, :email, :pay_type
	email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i 
	validates :email, presence: true, format: { with: email_regex }
	
	def addrelation(movie)

		self.relationships.create!(movie_id: movie) 
		
	end

	

end
