class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :order, dependent: :destroy

  def show_carts
  	cart_ids = $redis.smembers "cart#{id}"
    @cart_movies = User.find(cart_ids)
  end 

  def ordercount
    user=User.find(id)
    @orderscount=user.order.count
  end

  def cart_count
    $redis.scard "cart#{id}"
  end

  def moviesid
    $redis.smembers "cart#{id}"
  end

  
  
end
