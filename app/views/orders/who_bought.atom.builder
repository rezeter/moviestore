atom_feed do |feed|
  feed.title "Who bought ORDER N% #{@latest_order.id}"

  feed.updated @latest_order.try(:updated_at) 

  @orders.each do |order|
    feed.entry(order) do |entry|
      entry.title "Order #{order.id}"
      entry.summary type: 'xhtml' do |xhtml|
        xhtml.p "Shipped to #{order.address}"
        xhtml.table do
          xhtml.tr do
            xhtml.th 'Movie:'
            xhtml.th 'description:'
            xhtml.th 'Price:'
          end
          order.myorders.each do |movie|
            xhtml.tr do
              xhtml.td movie.title
              xhtml.td movie.description
              xhtml.td movie.price
            end
          end
         
        end
        xhtml.p "Paid by:  #{order.pay_type}"
      end
      entry.author do |author|
        author.name order.name
        author.email order.email
      end
    end
  end
end