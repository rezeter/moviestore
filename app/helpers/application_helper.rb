module ApplicationHelper
	
	include CurrentCart

	def hidden_div_if(condition, attributes = {}, &block)

		if condition
			attributes["style"] = "display: none"
		end
		content_tag("li", attributes, &block)
	end

    def total
    	cart_movies
    	return @cart_movies.to_a.sum { |movie| movie.price }
    end

    def total_order
    	movie = @order.myorders
    	return movie.to_a.sum { |movie| movie.price }
    end

    def total_orders(order)
    	movie=order.myorders 
    	return movie.to_a.sum { |movie| movie.price }
    end

end
