# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(window).load ->
  $('#mycart .fi-x').click (e) ->
    e.preventDefault()
    $this = $(this).closest('a')
    url = $this.data('targeturl')
    $.ajax url: url, type: 'put', success: (data) ->
      if data>=1 then $('#current_item').show('slow') else $('#current_item').hide() &&  location.href = "/"; 	
      $('.cart-count').html(data)
      $this.closest('.cart-movie').slideUp()
      $('#current_item').css({'background-color':'#987505'}).animate({'background-color':'#161817'}, 2000);
    
