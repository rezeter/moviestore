
if Rails.env == "development"
  $redis = Redis.new(:driver => :hiredis)
elsif Rails.env == "production"
 $redis = Redis.new(:url => ENV["REDIS_URL"])
end