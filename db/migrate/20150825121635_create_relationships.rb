class CreateRelationships < ActiveRecord::Migration
  def change
    create_table :relationships do |t|
      t.integer :order_id
      t.integer :movie_id

      t.timestamps null: false
    end

    add_index :relationships, :order_id
    add_index :relationships, :movie_id
    add_index :relationships, [:order_id, :movie_id], unique: true
  
  end
end
