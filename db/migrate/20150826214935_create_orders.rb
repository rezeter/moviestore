class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :name
      t.text :address
      t.string :email
      t.string :pay_type
      t.integer :user_id
      t.timestamps null: false
    end
    add_index :orders, [:user_id, :created_at]
  end
end
